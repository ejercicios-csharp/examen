public class Vehiculo
{
    private string marca;
    private string modelo;

    public Vehiculo(string marca, string modelo)
    {
        this.marca = marca;
        this.modelo = modelo;
    }

    public virtual void informacion()
    {
        Console.WriteLine("Marca: " + marca + ", Modelo: " + modelo);
    }
}