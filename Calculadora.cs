public class Calculadora
{
    private int num1;
    private int num2;

    public Calculadora(int num1, int num2)
    {
        this.num1 = num1;
        this.num2 = num2;
    }

    public string suma()
    {
        return num1 + " + " + num2 + ": " + (num1 + num2);
    }

    public string resta()
    {
        return num1 + " - " + num2 + ": " + (num1 - num2);
    }

    public string multiplicar()
    {
        return num1 + " x " + num2 + ": " + (num1 * num2);
    }

    public string dividir()
    {
        if (num1 == 0)
        {
            return "No se puede dividir 0";
        }
        else if (num2 == 0)
        {
            return "No se puede dividir entre 0";
        }
        else
        {
            return num1 + " / " + num2 + ": " + (num1 / num2);
        }
    }
}