public class Automovil : Vehiculo
{
    private string marca;
    private string modelo;
    private string anio;

    public Automovil(string marca, string modelo, string anio) : base(marca, modelo)
    {
        this.anio = anio;
    }

    public void informacion()
    {
        Console.WriteLine("Marca: " + marca + ", Modelo: " + modelo + ", A�o: " + anio);
    }
}