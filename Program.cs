﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;


public class examen
{
    public static void Main(string[] args)
    {
        int opcion;
        int num;

        do
        {
            Console.WriteLine("0 - Salir");
            Console.WriteLine("1 - Suma de array");
            Console.WriteLine("2 - Calculadora");
            Console.WriteLine("3 - Invertir cadena");
            Console.WriteLine("4 - Comprobar palindromo");

            Console.Write("Opcion: ");
            opcion = int.TryParse(Console.ReadLine(), out opcion) ? opcion : 5; //Pide un numero, si no es numero parseable, devuelve 4
            switch (opcion)
            {
                case 0:
                    break;
                case 1:
                    Console.WriteLine("Total: " + sumarLista(crearListaInt()));
                    break;
                case 2:
                    menuCalculadora();
                    break;
                case 3:
                    Console.Write("Introduce la candena: ");
                    Console.WriteLine(invertirCadena(Console.ReadLine()));
                    break;
                case 4:
                    Console.Write("Introduce la candena: ");
                    verificarPalindromo(Console.ReadLine());
                    break;
                default:
                    Console.WriteLine("Opcion incorrecta");
                    break;
            }

            continuar();
        } while (opcion != 0);
    }

    //Crea una lista List<int> y la devuelve
    public static List<int> crearListaInt()
    {
        List<int> listaInt = new List<int> { };

        Console.WriteLine("Introduce 5 numeros");

        do
        {
            listaInt.Add(pideInt(""));
        } while(listaInt.Count() < 5);

        return listaInt;
    }

    //Pide una string e intenta parsearla a int, si puede, devuelve el numero, si no puede, lo pide otra vez
    public static int pideInt(string linea)
    {
        bool valido = false;
        int num = 0;

        do
        {
            try
            {
                Console.Write(linea);
                num = int.Parse(Console.ReadLine());
                valido = true;
            } catch(FormatException fe)
            {
                Console.WriteLine("Introduce un numero valido");
            }
            
        } while (!valido);

        return num;
    }

    //Menu que recoge todas las opciones de la calculadora
    public static void menuCalculadora() {
        Calculadora calculadora = new Calculadora(pideInt("Introduce el primer numero"), pideInt("Introduce el segundo numero"));

        int opcion;

        do
        {
            Console.WriteLine("0 - Salir");
            Console.WriteLine("1 - Sumar");
            Console.WriteLine("2 - Restar");
            Console.WriteLine("3 - Multiplicar");
            Console.WriteLine("4 - Dividir");

            Console.Write("Opcion: ");
            opcion = int.TryParse(Console.ReadLine(), out opcion) ? opcion : 5; //Pide un numero, si no es numero devuelve 4
            switch (opcion)
            {
                case 0:
                    break;
                case 1:
                    Console.WriteLine(calculadora.suma());
                    break;
                case 2:
                    Console.WriteLine(calculadora.resta());
                    break;
                case 3:
                    Console.WriteLine(calculadora.multiplicar());
                    break;
                case 4:
                    Console.WriteLine(calculadora.dividir());
                    break;
                default:
                    Console.WriteLine("Opcion incorrecta");
                    break;
            }

            continuar();
        } while (opcion != 0);
    }

    //Metodo para limpiar la consola
    public static void continuar()
    {
        Console.WriteLine("Pulsa intro para continuar");
        Console.ReadLine();
        Console.Clear();
    }

    //1. Manipulacion de Arrays
    public static int sumarLista(List<int> listaInt)
    {
        int total = 0;

        foreach (var item in listaInt)
        {
            total += item;
        }

        return total;
    }        

    //4. Manipulacion de Cadenas
    public static string invertirCadena(string cadena)
    {
        string[] palabras = cadena.Split();
        Array.Reverse(palabras);
        string resultado = string.Join(" ", palabras);
        return resultado;
    }

    //5. Manipulacion de Cadenas y Expresiones Regulares
    public static void verificarPalindromo(string cadena1)
    {
        string cadena2 = "";

        for (int i = (cadena1.Length - 1); i >= 0 ; i--)
        {
            cadena2 += cadena1[i];
        }

        if(cadena2 == cadena1)
        {
            Console.WriteLine("Es palindroma");
        } else
        {
            Console.WriteLine("Es palindroma");
        }
    }
}